Module: 'menucopy'
Version: 7.x-1.1
kris@o3world.com
dec 5 '12

When creating a new custom menu via the Add Menu form, this module
provides an option to copy all of an existing menu's items to it.
